import numpy as np 
from bokeh.io import curdoc, show
from bokeh.layouts import column, row, widgetbox, layout
from bokeh.models import ColumnDataSource, Select, Div
from bokeh.plotting import figure
from bokeh.transform import cumsum
from bokeh.models.widgets import DataTable, DateFormatter, TableColumn, HTMLTemplateFormatter
from styles import *

"""To run set terminal at level above monitor-app and run:

bokeh serve --show monitor-app

"""

def get_device_states_data(dict_data):
    device_name, device_state = device_states_to_columns(dict_data)
    device_states_data ={'device_name': device_name, 'device_state': device_state}
    return ColumnDataSource(data = device_states_data)

def get_state_donut_data(dict_data):
    named_device_states = ['ON','RESOURCING','IDLE','OFF','ERROR']
    names, states = device_states_to_columns(dict_data)
    angle_vals = [(states.count(x)/float(len(states)))*(2.0*np.pi) for x in named_device_states]

    donut_data = {'angle': angle_vals, 
        'colour': ['green','yellow','grey','purple','red'],
        'state':['ON','RESOURCING','IDLE','OFF','ERROR']}

    return ColumnDataSource(data = donut_data)


def state_donut_plot(donut_data):

    donut = figure(plot_height=400, title="", toolbar_location=None, x_range=(-.5, .5), tooltips="@state")
    donut.annular_wedge(x=0, y=1,  inner_radius=0.15, outer_radius=0.25, direction="anticlock",
        start_angle=cumsum('angle', include_zero=True), end_angle=cumsum('angle'),
        line_color="white", fill_color='colour', legend='state', source=donut_data)
    donut.axis.axis_label=None
    donut.axis.visible=False
    donut.grid.grid_line_color = None
    return donut

def device_states_to_columns(dict):
    """"""
    device_name = []
    device_state = []
    for key in dict:
        device_name.append(key) 
        device_state.append(dict[key])

    return device_name, device_state

def update_dashboard(attrname, old, new):
    dict_entry = state_select.value
    dict_data = test_dict[dict_entry]['devices_states']
    src = get_device_states_data(dict_data)
    device_states_source.data.update(src.data)
    src_don = get_state_donut_data(dict_data)
    device_donut_source.data.update(src_don.data)


test_dict = {
    'allOn':{
        'devices_states': {
            'ska_mid/tm_central/central_node:state': 'ON', 
            'ska_mid/tm_subarray_node/1:state': 'ON', 
            'ska_mid/tm_subarray_node/2:state': 'ON', 
            'ska_mid/tm_subarray_node/3:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_master:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_master:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_subarray01:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_subarray02:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_subarray03:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray01:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray02:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray03:state': 'ON', 
            'ska_mid/tm_leaf_node/d0001:state': 'ON', 
            'ska_mid/tm_leaf_node/d0002:state': 'ON', 
            'ska_mid/tm_leaf_node/d0003:state': 'ON', 
            'ska_mid/tm_leaf_node/d0004:state': 'ON', 
            'ska_mid/tm_central/central_node:telescopestate': 'ON', 
            'mid-csp/control/0:state': 'ON', 
            'mid-csp/subarray/01:state': 'ON', 
            'mid-csp/subarray/02:state': 'ON', 
            'mid-csp/subarray/03:state': 'ON', 
            'ska_mid/tm_subarray_node/1:obsstate': 'ON', 
            'mid-sdp/subarray/01:obsstate': 'ON', 
            'mid-csp/subarray/01:obsstate': 'ON', 
            'mid-sdp/control/0:state': 'ON', 
            'mid-sdp/subarray/01:state': 'ON', 
            'mid-sdp/subarray/02:state': 'ON', 
            'mid-sdp/subarray/03:state': 'ON'}, 
        'current_spectrum': 'Unknown'
        },
    'onIdleOff':{
        'devices_states': {
            'ska_mid/tm_central/central_node:state': 'ON', 
            'ska_mid/tm_subarray_node/1:state': 'ON', 
            'ska_mid/tm_subarray_node/2:state': 'ON', 
            'ska_mid/tm_subarray_node/3:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_master:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_master:state': 'ON', 
            'ska_mid/tm_leaf_node/csp_subarray01:state': 'OFF', 
            'ska_mid/tm_leaf_node/csp_subarray02:state': 'OFF', 
            'ska_mid/tm_leaf_node/csp_subarray03:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray01:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray02:state': 'ON', 
            'ska_mid/tm_leaf_node/sdp_subarray03:state': 'OFF', 
            'ska_mid/tm_leaf_node/d0001:state': 'ON', 
            'ska_mid/tm_leaf_node/d0002:state': 'IDLE', 
            'ska_mid/tm_leaf_node/d0003:state': 'IDLE', 
            'ska_mid/tm_leaf_node/d0004:state': 'IDLE', 
            'ska_mid/tm_central/central_node:telescopestate': 'ON', 
            'mid-csp/control/0:state': 'ON', 
            'mid-csp/subarray/01:state': 'ON', 
            'mid-csp/subarray/02:state': 'ON', 
            'mid-csp/subarray/03:state': 'ON', 
            'ska_mid/tm_subarray_node/1:obsstate': 'ON', 
            'mid-sdp/subarray/01:obsstate': 'ON', 
            'mid-csp/subarray/01:obsstate': 'ON', 
            'mid-sdp/control/0:state': 'ON', 
            'mid-sdp/subarray/01:state': 'ON', 
            'mid-sdp/subarray/02:state': 'ON', 
            'mid-sdp/subarray/03:state': 'ON'
            }, 
        'current_spectrum': 'Unknown'
        }
}


#--- setup the formatting
formatter =  HTMLTemplateFormatter(template=template_complex)

#--- set data source
dict_entry = 'allOn'
dict_data = test_dict[dict_entry]['devices_states']
device_states_source = get_device_states_data(dict_data)
device_donut_source = get_state_donut_data(dict_data)

#-- create table
device_states_columns = [TableColumn(field = 'device_name', title="Device Name", width = 400),
                        TableColumn(field = 'device_state', title="Device State", width = 200, formatter=formatter)]


state_donut = state_donut_plot(device_donut_source)

# #--- define data table
data_table = DataTable(source=device_states_source,
                       columns=device_states_columns,
                       autosize_mode = 'fit_columns',
                       selectable = True,
                       sortable = True,
                       width=600,height=1000)


#------------------#
#   COSMETICS      #
#------------------#
#--- Create a header DIV

header =  Div(text="""SKAO DASHBOARD""",
width=200, height=100, align='center', name='header_div', css_classes=['header'])

footer =  Div(text="""SKAO DASHBOARD Footer""",
width=200, height=100, align='center', name='footer_div', css_classes=['footer']) #--- this will only owrk if its a full bokeh server thing... so not now

#------------------#
#   LAYOUT         #
#------------------#
#https://docs.bokeh.org/en/latest/docs/reference/models/widgets.tables.html
#https://stackoverflow.com/questions/50996875/how-to-color-rows-and-or-cells-in-a-bokeh-datatable
# https://malouche.github.io/notebooks/donut_with_bokeh2.html


state_select = Select(value = dict_entry, title='Select System State (Just for testing!)',options = sorted(test_dict.keys()))

state_select.on_change('value', update_dashboard)

toggles = column(state_donut,state_select)
datarow = row(toggles,data_table)
dash  = layout(children=[datarow], name='dash')
curdoc().add_root(header)
curdoc().add_root(dash)
curdoc().add_root(footer)