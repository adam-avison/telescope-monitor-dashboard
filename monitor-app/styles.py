template_simple="""
            <div style="background:<%= 
                (function colorfromint(){
                    if(device_state == 'ON' ){
                        return("green")}
                    }()) %>; 
                color: black; text-align: center"> 
            <%= value %>
            </div>
            """


template_complex="""
            <div style="background:<%= 
                (function colorfromint(){
                    if (device_state == 'ON' ){
                        return("green")
                        }
                    else if (device_state == 'IDLE'){
                        return("grey")
                        }
                    else if (device_state == 'RESOURCING'){
                        return("yellow")
                        }
                    else if (device_state == 'ERROR'){
                        return("red")
                        }
                    else if (device_state == 'OFF'){
                        return("purple")
                        }
                    }()) %>; 
                color:<%= 
                (function colorfromint(){
                    if (device_state == 'ON' ){
                        return("white")
                        }
                    else if (device_state == 'IDLE'){
                        return("white")
                        }
                    else if (device_state == 'RESOURCING'){
                        return("black")
                        }
                    else if (device_state == 'ERROR'){
                        return("white")
                        }
                    else if (device_state == 'OFF'){
                        return("white")
                        }
                    }()) %>; 
                text-align: center"> 
            <%= value %>
            </div>
            """